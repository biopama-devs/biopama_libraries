/**
 * 
 * This is for the basic standard layers that should be the same in EVERY map. If it's a custom layer you can add it normally with the mapbox API in your own code somewhere else.
 * I'm adding the map as an argument just incase we get multiple maps one day.
 * 
 */

(function($){
    $.fn.addMapLayerBiopamaSources = function(map){
        $.fn.attachMapEventHandler(
            map,
            'load',
            function(){
                _addMapLayerBiopamaSources(map);
            }
        );
    }

	_addMapLayerBiopamaSources = function(map){
        map.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [biopamaGlobal.map.hostURL.poly+"/{z}/{x}/{y}.pbf"], 
		});
		map.addSource("BIOPAMA_Point", {
			"type": 'vector',
			"tiles": [biopamaGlobal.map.hostURL.point+"/{z}/{x}/{y}.pbf"],
		});
        map.addSource('geospatial_jrc', {
            type: 'vector',
            tiles: [
                'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=marxan:wdpa_latest_biopama&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/x-protobuf;type=mapbox-vector&TileMatrix=EPSG:900913:{z}&TILECOL={x}&TILEROW={y}'
            ],
            'tileSize': 512,
            'scheme': 'xyz',
        });
        map.addSource("panorama", {
            type: "geojson",
            data: {
                type: "FeatureCollection",
                crs: { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
                features: [],
            },
        });
    }

    $.fn.addMapLayer = function(map, layerID, nodeID = 0){
        $.fn.attachMapEventHandler(
            map,
            'load',
            function(){
                _addMapLayer(map, layerID, nodeID);
            }
        );
    }

    _addMapLayer = function(map, layerID, nodeID){
        //just checking if the layer is already in the map. If it is we skip the function.
        var currentLayers = map.style._layers;
	    for (var key in currentLayers) { 
            if (currentLayers[key].id.indexOf(layerID) == false) {
                return;
            }
        }
        const layers = map.getStyle().layers;
        let firstSymbolId;
        for (const layer of layers) {
            if (layer.type === 'symbol') {
                firstSymbolId = layer.id;
                break;
            }
        }

        /**
         * 
         * Each "Layer" consist of multiple layers. 
         * EG. The country layer has a hover layer and a highlighted layer as well as a points layer for the label. 
         * 
         */
        switch(layerID){
            case "base":
                map.addLayer({
                    'id': 'light',
                    'type': 'raster',
                    'source': {
                      'id': 'light-source',
                      'type': 'raster',
                      'tiles': ['https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiamFtZXNkYXZ5IiwiYSI6ImNpenRuMmZ6OTAxMngzM25wNG81b2MwMTUifQ.A2YdXu17spFF-gl6yvHXaw'],
                      'tileSize': 256,
                      'scheme': 'xyz',
                    },
                    'paint': {},
                    "layout": {
                        "visibility": "none"
                    }
                });
                map.addLayer({
                    'id': 'dark',
                    'type': 'raster',
                    'source': {
                      'id': 'light-source',
                      'type': 'raster',
                      'tiles': ['https://api.mapbox.com/styles/v1/mapbox/dark-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiamFtZXNkYXZ5IiwiYSI6ImNpenRuMmZ6OTAxMngzM25wNG81b2MwMTUifQ.A2YdXu17spFF-gl6yvHXaw'],
                      'tileSize': 256,
                      'scheme': 'xyz',
                    },
                    'paint': {},
                    "layout": {
                        "visibility": "none"
                    }
                });
                break;
            case "biopamaGaulEez":
                map.addLayer({
                    "id": "gaulACP",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.GAUL,
                    "maxzoom": 6,
                    "paint": {"line-color": "#aaa",
                        "line-width": 1,}
                });

                map.addLayer({
                    "id": "BIOPAMA",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.EEZ,
                    "maxzoom": 6,
                    "paint": {"fill-color": "hsl(224, 39%, 73%)", "fill-opacity": 0.3}
                });
                break;
            case "biopamaRegions":
                map.addLayer({
                    "id": "regionMask",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.subRegion,
                    "paint": {
                        'fill-color': '#fff',
                        'fill-outline-color': '#fff',
                        "fill-opacity": 0.01,
                    }
                });
                map.addLayer({
                    "id": "regionBorder",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.subRegion,
                    "paint": {
                        "line-color": "#75b329",
                        "line-width": ["interpolate", ["linear"], ["zoom"], 0, 4, 5, 1]
                    }
                });
                map.addLayer({
                    "id": "regionHover",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.subRegion,
                    "layout": {
                        "visibility": "none",
                    },
                    "paint": {
                        "line-color": biopamaGlobal.map.colors.region,
                        "line-width": 4,
                    }
                });
                map.addLayer({
                    "id": "regionLabels",
                    "type": "symbol",
                    "source": "BIOPAMA_Point",
                    "source-layer": biopamaGlobal.map.layers.subRegionPoint,
                    "maxzoom": 4,
                    "layout": {
                        "text-field": ["to-string", ["get", "Group"]],
                        "text-font": [
                            "Arial Unicode MS Regular"
                        ],
                        "text-size": 22,
                        "text-ignore-placement": true,
                        "text-allow-overlap": true
                    },
                    "paint": {
                        "text-color": "#a25b28",
                        "text-halo-width": 3,
                        "text-halo-color": "#fff",
                        "text-halo-blur": 4
                    }
                });
                map.addLayer({
                    "id": "regionSelected",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.subRegion,
                    "layout": {
                        "visibility": "none",
                    },
                    "paint": {
                        "line-color": biopamaGlobal.map.colors.region,
                        "line-dasharray": [3, 1],
                        "line-width": 3,
                    }
                });
                map.on('mousemove', "regionMask", function (e) {
                    regionCurrentlyHovered = '';
                     if (e.features.length > 0) {
                        regionCurrentlyHovered = e.features[0].properties.Group;
                        $('#map-region-info').text("Region: "+ regionCurrentlyHovered);
                        if(regionCurrentlyHovered == selSettings.regionID){
                            map.setLayoutProperty("regionHover", 'visibility', 'none');
                        } else {
                            map.setFilter('regionHover', ['==', 'Group', regionCurrentlyHovered]);		
                            map.setLayoutProperty("regionHover", 'visibility', 'visible');
                            if ($(".indicator-chart:visible").length){
                                highlightMapFeature();
                            }					
                        }
                    } 
                });
                map.on("mouseleave", "regionMask", function() {
                    regionCurrentlyHovered = '';
                    $('#map-region-info').empty();
                    map.setLayoutProperty("regionHover", 'visibility', 'none');
                });

                break;
            case "biopamaCountries":
                map.addLayer({
                    "id": "countryBorders",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "paint": {
                        "line-color": "#444",
                        "line-width": 1,
                    }
                });
                map.addLayer({
                    "id": "countryMask",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "paint": {
                        'fill-color': '#fff',
                        "fill-opacity": 0.01,
                    }
                });
                map.addLayer({
                    "id": "countryFill",
                    "type": "fill",
                    "layout": {
                        "visibility": "none"
                    },
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "paint": {
                        'fill-color': '#fff',
                        'fill-outline-color': '#fff',
                        "fill-opacity": 0.6,
                    }
                });
                map.addLayer({
                    "id": "countryHover",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none",
                    },
                    "paint": {
                        "line-color": biopamaGlobal.map.colors.country,
                        "line-width": 2,
                    }
                });
                map.addLayer({
                    "id": "countrySelected",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none"
                    },
                    "paint": {
                        "line-color": biopamaGlobal.map.colors.country,
                        "line-width": 3
                    }
                });

                map.on('mousemove', "countryMask", function (e) {
                    countryCurrentlyHoverediso2 = '';
                    countryCurrentlyHoverediso3 = '';
                    countryCurrentlyHoveredUn = '';
                    if (e.features.length > 0) {
                        $(".map-country-pa-info-wrapper").show();
                        $('#map-country-info').text("Country: "+ e.features[0].properties.original_n);
                        if(e.features[0].properties.iso2 == selSettings.iso2){
                            map.setLayoutProperty("countryHover", 'visibility', 'none');
                        } else {
                            countryCurrentlyHoverediso2 = e.features[0].properties.iso2;
                            countryCurrentlyHoverediso3 = e.features[0].properties.iso3;
                            countryCurrentlyHoveredUn = e.features[0].properties.un_m49;
                            map.setFilter('countryHover', ['==', 'iso3', e.features[0].properties.iso3]);		
                            map.setLayoutProperty("countryHover", 'visibility', 'visible');
                            if ($(".indicator-chart:visible").length){
                                highlightMapFeature();
                            }	
                        }
                    } 
                });
                map.on("mouseleave", "countryMask", function() {
                    $(".map-country-pa-info-wrapper").hide();
                    countryCurrentlyHoverediso2 = '';
                    countryCurrentlyHoverediso3 = '';
                    countryCurrentlyHoveredUn = '';
                    $('#map-country-info').empty();
                    map.setLayoutProperty("countryHover", 'visibility', 'none');
                });
                break;
            case "biopamaWDPAPoly":
                map.addLayer({
                    "id": "wdpaAcpMask",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.pa,
                    "minzoom": 3,
                    "paint": {
                        'fill-color': '#fff',
                        "fill-opacity": 0.01
                    }
                });
                map.addLayer({
                    "id": "wdpaAcpFill",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.pa,
                    "minzoom": 3,
                    "paint": {
                        "fill-color": [
                            "match",
                            ["get", "MARINE"],
                            ["1"],
                            "hsla(173, 21%, 51%, 0.2)",
                            "hsla(87, 47%, 53%, 0.2)"
                        ],
                        "fill-opacity": [
                            "interpolate",
                            ["exponential", 1],
                            ["zoom"],
                            3,
                            0.3,
                            5,
                            0.5,
                            6,
                            1
                        ]
                    }
                });
                map.addLayer({
                    "id": "wdpaAcpFillHighlighted",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.pa,
                    "layout": {
                        "visibility": "none"
                    },
                    'paint': {
                        'fill-color': biopamaGlobal.map.colors.pa,
                        'fill-outline-color': biopamaGlobal.map.colors.pa,
                        "fill-opacity": 0.6
                    }
                });
                map.addLayer({
                    "id": "wdpaAcpHover",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.pa,
                    "layout": {
                        "visibility": "none"
                    },
                    "paint": {
                        "line-color": "#8fc04f",
                        "line-width": 3,
                    }
                });
                map.addLayer({
                    "id": "wdpaAcpSelected",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.pa,
                    "layout": {
                        "visibility": "none"
                    },
                    "paint": {
                        "line-color": "#679b95",
                        "line-width": 2,
                    },
                    "transition": {
                      "duration": 300,
                      "delay": 0
                    }
                });
                //we seperate polygon and point layers to have an offset for the points, allowing the user to still see the point and the label at thte same time.
                map.addLayer({
                    "id": "wdpaAcpPolyLabels",
                    "type": "symbol",
                    "source": "BIOPAMA_Point",
                    "source-layer": biopamaGlobal.map.layers.paLabels,
                    "minzoom": 5,
                    "layout": {
                        "text-field": ["to-string", ["get", "NAME"]],
                        "text-size": 12,
                        "text-font": [
                            "Arial Unicode MS Regular",
                            "Arial Unicode MS Regular"
                        ]
                    },
                    "paint": {
                        "text-halo-width": 2,
                        "text-halo-blur": 2,
                        "text-halo-color": "hsl(0, 0%, 100%)",
                        "text-opacity": 1
                    }
                });
                map.on('mousemove', "wdpaAcpMask", function (e) {
                    pasCurrentlyHovered = [];
                    paText = 'Protected Area: '
                     if (e.features.length > 0) {
                        for (var key in e.features) {
                            if(!pasCurrentlyHovered.includes(e.features[key].properties.wdpaid)){
                                pasCurrentlyHovered.push(e.features[key].properties.wdpaid);
                            }
                            //print the 
                            paText = paText + e.features[key].properties.NAME + '<br> ';
                            
                            //check if the currently selected PA in in the array if it's found, remove it (so we are not going to highlight the currently selected PA on hover)
                            var filteredAry = pasCurrentlyHovered.filter(e => e !== selSettings.WDPAID)
                            map.setFilter("wdpaAcpHover", buildFilter(filteredAry, 'in', 'wdpaid'));	
                            map.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');					
                        }
                        //print the names of the PAs
                        $('#map-pa-info').html(paText);
                    } 
                });
                map.on("mouseleave", "wdpaAcpMask", function() {
                    pasCurrentlyHovered = [];
                    $('#map-pa-info').empty();
                    map.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
                });
                break;
            case "biopamaWDPAPolyJRC":
                    map.addLayer({
                        "id": "wdpaAcpMask",
                        "type": "fill",
                        //"source": "BIOPAMA_Poly",
                        //"source-layer": biopamaGlobal.map.layers.pa,
                        "source": "geospatial_jrc",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest,
                        "minzoom": 2,
                        "paint": {
                            'fill-color': '#fff',
                            "fill-opacity": 0.01
                        }
                    });
                    map.addLayer({
                        "id": "wdpaAcpFill",
                        "type": "fill",
                        "source": "geospatial_jrc",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest, //wdpa_latest_biopama
                        "minzoom": 2,
                        "paint": {
                            "fill-color": [
                                "match",
                                ["get", "MARINE"],
                                ["1"],
                                "hsla(173, 21%, 51%, 0.2)",
                                "hsla(87, 47%, 53%, 0.2)"
                            ],
                            "fill-opacity": [
                                "interpolate",
                                ["exponential", 1],
                                ["zoom"],
                                3,
                                0.3,
                                5,
                                0.5,
                                6,
                                1
                            ]
                        }
                    });
                    map.addLayer({
                        "id": "wdpaAcpFillHighlighted",
                        "type": "fill",
                        //"source": "BIOPAMA_Poly",
                        //"source-layer": biopamaGlobal.map.layers.pa,
                        "source": "geospatial_jrc",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest,
                        "layout": {
                            "visibility": "none"
                        },
                        'paint': {
                            'fill-color': biopamaGlobal.map.colors.pa,
                            'fill-outline-color': biopamaGlobal.map.colors.pa,
                            "fill-opacity": 0.6
                        }
                    });
                    map.addLayer({
                        "id": "wdpaAcpHover",
                        "type": "line",
                        "source": "geospatial_jrc",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest,
                        //"source": "BIOPAMA_Poly",
                        //"source-layer": biopamaGlobal.map.layers.pa,
                        "layout": {
                            "visibility": "none"
                        },
                        "paint": {
                            "line-color": "#8fc04f",
                            "line-width": 3,
                        }
                    });
                    map.addLayer({
                        "id": "wdpaAcpSelected",
                        "type": "line",
                        "source": "geospatial_jrc",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest,
                        //"source": "BIOPAMA_Poly",
                        //"source-layer": biopamaGlobal.map.layers.pa,
                        "layout": {
                            "visibility": "none"
                        },
                        "paint": {
                            "line-color": "#679b95",
                            "line-width": 2,
                        },
                        "transition": {
                          "duration": 300,
                          "delay": 0
                        }
                    });
                    //we seperate polygon and point layers to have an offset for the points, allowing the user to still see the point and the label at thte same time.
                    map.addLayer({
                        "id": "wdpaAcpPolyLabels",
                        "type": "symbol",
                        "source": "BIOPAMA_Point",
                        "source-layer": biopamaGlobal.map.layers.paLabels,
                        "minzoom": 5,
                        "layout": {
                            "text-field": ["to-string", ["get", "NAME"]],
                            "text-size": 12,
                            "text-font": [
                                "Arial Unicode MS Regular",
                                "Arial Unicode MS Regular"
                            ]
                        },
                        "paint": {
                            "text-halo-width": 2,
                            "text-halo-blur": 2,
                            "text-halo-color": "hsl(0, 0%, 100%)",
                            "text-opacity": 1
                        }
                    });
                    map.on('mousemove', "wdpaAcpMask", function (e) {
                        pasCurrentlyHovered = [];
                        paText = 'Protected Area: '
                         if (e.features.length > 0) {
                            for (var key in e.features) {
                                if(!pasCurrentlyHovered.includes(e.features[key].properties.wdpaid)){
                                    pasCurrentlyHovered.push(e.features[key].properties.wdpaid);
                                }
                                //print the 
                                paText = paText + e.features[key].properties.NAME + '<br> ';
                                
                                //check if the currently selected PA in in the array if it's found, remove it (so we are not going to highlight the currently selected PA on hover)
                                var filteredAry = pasCurrentlyHovered.filter(e => e !== selSettings.wdpaid)
                                map.setFilter("wdpaAcpHover", buildFilter(filteredAry, 'in', 'wdpaid'));	
                                map.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');					
                            }
                            //print the names of the PAs
                            $('#map-pa-info').html(paText);
                        } 
                    });
                    map.on("mouseleave", "wdpaAcpMask", function() {
                        pasCurrentlyHovered = [];
                        $('#map-pa-info').empty();
                        map.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
                    });
                    break;
            case "biopamaWDPAPoint":
                map.addLayer({
                    "id": "wdpaAcpPoint",
                    "type": "circle",
                    "source": "BIOPAMA_Point",
                    "source-layer": biopamaGlobal.map.layers.paPoint,
                    "filter": ["match", ["get", "Point"], [1], true, false],
                    "minzoom": 5,
                    "paint": {
                        "circle-color": [
                            "match",
                            ["get", "MARINE"],
                            ["1"],
                            "hsla(173, 21%, 51%, 0.2)",
                            "hsla(87, 47%, 53%, 0.2)"
                        ]
                    }
                });
                map.addLayer({
                    "id": "wdpaAcpPointLabels",
                    "type": "symbol",
                    "source": "BIOPAMA_Point",
                    "source-layer": biopamaGlobal.map.layers.paPoint,
                    "minzoom": 5,
                    "layout": {
                        "text-field": "{NAME}",
                        "text-size": 12,
                        "text-padding": 3,
                        "text-offset": [0,-1]
                    },
                    "paint": {
                        "text-color": "hsla(213, 49%, 13%, 0.95)",
                        "text-halo-color": "hsla(0, 0%, 100%, .9)",
                        "text-halo-width": 2,
                        "text-halo-blur": 2
                    }
                });
                break;
            case "satellite":
                map.addLayer({
                    id: 'satellite',
                    source: {"type": "raster",  "url": "mapbox://mapbox.satellite", "tileSize": 256},
                    type: "raster",
                    "layout": {
                              "visibility": "none"
                          }
                }, firstSymbolId);
                break;
            case 'CountriesRedGreen':
                map.addLayer({
                    "id": "CountriesGreenMask",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none"
                    },
                    'paint': {
                        'fill-color': '#8fc04f',
                        "fill-opacity": 0.5
                    }
                });
                map.addLayer({
                    "id": "CountriesRedMask",
                    "type": "fill",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none"
                    },
                    'paint': {
                        'fill-color': '#ae0000',
                        "fill-opacity": 0.5
                    }
                });
                break;
            case 'CountriesGoodBad':
                map.addLayer({
                    "id": "CountriesBadMask",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none"
                    },
                    'paint': {
                        "line-color": "rgba(255,255,255, 0.1)",
                        "line-width": 3
                    }
                });
                map.addLayer({
                    "id": "CountriesGoodMask",
                    "type": "line",
                    "source": "BIOPAMA_Poly",
                    "source-layer": biopamaGlobal.map.layers.country,
                    "layout": {
                        "visibility": "none"
                    },
                    'paint': {
                        "line-color": "#8FBF4B",
                        "line-width": 3
                    }
                });
                break;
            case "nan-layers":
                map.loadImage('/themes/custom/biopama/img/nan-pattern.png', function(err, image) {
                    // Throw an error if something went wrong
                    if (err) throw err;
                    map.addImage('pattern', image);
                    map.addLayer({
                        "id": "country-nan-layer-"+nodeID,
                        "type": "fill",
                        "source": "BIOPAMA_Poly",
                        "source-layer": biopamaGlobal.map.layers.country,
                        "paint": {
                            "fill-pattern": "pattern"
                        },
                        "layout": {
                            "visibility": "none"
                        },
                    }, firstSymbolId);

                    map.addLayer({
                        "id": "pa-nan-layer-"+nodeID,
                        "type": "fill",
                        "source": "BIOPAMA_Poly",
                        "source-layer": biopamaGlobal.map.layers.WDPALatest,
                        "paint": {
                            "fill-pattern": "pattern"
                        },
                        "layout": {
                            "visibility": "none"
                        },
                    }, firstSymbolId);
                });
                break;
            case "akp":
                var url_afica_mask = "https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:mask_akp@EPSG:900913@png/{z}/{x}/{y}.png"
                map.addLayer({
                    'id': 'afica_mask',
                    'type': 'raster',
                    'source': {
                        'id': 'afica_mask',
                        'type': 'raster',
                        'tiles': [url_afica_mask],
                        'tileSize': 256,
                        'scheme': 'tms',
                    },
                    'paint': {"raster-opacity" : 1}
                }, 'countrySelected');
                var url_dotted = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=africa_platform%3Adotted_ws&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=35&SRS=EPSG:3857&BBOX={bbox-epsg-3857}"
                map.addLayer({
                    'id': "dotted",
                    'type': 'raster',
                    'source': {
                        'id': "eez",
                        'type': 'raster',
                        'tiles': [url_dotted],
                        'tileSize': 256,
                        'scheme': "xyz",
                    },
                    'paint': {}
                });
                map.addSource('mapbox-dem', {
                    'type': 'raster-dem',
                    'url': 'mapbox://mapbox.mapbox-terrain-dem-v1',
                    'tileSize': 512,
                    'maxzoom': 14
                });
                    // add the DEM source as a terrain layer with exaggerated height
                map.setTerrain({ 'source': 'mapbox-dem', 'exaggeration': 5 });

                //var url_country_borders = "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms?service=WMS&version=1.1.1&request=GetMap&layers=africa_platform%3Aap_country_stats&bbox={bbox-epsg-3857}&width=256&height=256&srs=EPSG:3857&format=image%2Fpng&styles=white_polygon_t&TRANSPARENT=true"

                // map.addLayer({
                //     'id': "country_borders",
                //     'type': 'raster',
                //     'source': {
                //         'id': "eez",
                //         'type': 'raster',
                //         'tiles': [url_country_borders],
                //         'tileSize': 256,
                //         'scheme': "xyz",
                //     },
                //     'paint': {}
                // }, 'countrySelected');
                break;
            default:
                console.log("layer: " + layerID + " failed to load for some reason. Check it out.");     
        }
       
    }

    $.fn.removeMapLayer = function(map, layerID){
        var currentLayers = map.style._layers;
	    for (var key in currentLayers) { 
            if (currentLayers[key].id.indexOf(layerID) == true) {
                map.removeLayer(currentLayers[key].id);
            }
        }
    }

    $.fn.setLayerVisibility = function(map, layerId, isVisible){
        let visibility = isVisible? 'visible' : 'none';
        map.setLayoutProperty(layerId, 'visibility', visibility);
    }
    function buildFilter(arr, arg, filter) {
        var filter = [arg, filter];
        if (arr.length === 0) {
          return filter;
        }
        for(var i = 0; i < arr.length; i += 1) {
          filter.push(arr[i]);
        }
        return filter;
      }
})(jQuery);