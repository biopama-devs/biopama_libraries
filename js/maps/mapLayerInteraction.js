var closePopupIcon;

(function($){

    $.fn.addMapLayerInteraction = function(map, popupOptions = {}){
        /**
         * These options are to allow the URLs in the popup to be controlled depending on what module is invoking the function
         * If a function should be run on like click set the Link value to '#' and create an onClick event where you need it.
            popupOptions = {
                regionLink: '',
                countryLink: '',
                paLink: '',
            }
         * 
         */
        
        var theseOptions = {
            region: {
                url: popupOptions.regionLink || '#', 
            },
            country: {
                url: popupOptions.countryLink || '#', 
            },
            pa: {
                url: popupOptions.paLink || '#', 
            }, 
        }
        
        $.fn.attachMapEventHandler(
            map,
            'load',
            function(){
                _addMapLayerInteraction(map, theseOptions);
            }
        );
    }
    _addMapLayerInteraction = function(map, theseOptions){

        map.on('click', getFeatureInfo);
	
        var popUpContents = {};
    
        function getFeatureInfo(e){
            popUpContents = {
                region: '',
                regionID: '',
                country: '',
                countryCode: '',
                country3Code: '',
                PAs: [],
                WDPAIDs: [],
                paIUCNCats: [],
                paSize: [],
            };
            new Promise(function(resolve, reject) {
            regionCheck(e)
            }).then(countryCheck(e))
            .then(paCheck(e))
            .then(makePopUp(e));
        }

        function makePopUp(e){
            if (popUpContents.regionID.length){
                var areaCheck = "areaInactive";
                if (selSettings.regionName == popUpContents.region)areaCheck = "areaActive";
                var popup = popUpSection(areaCheck, popUpContents.region, popUpContents.regionID, "region");
                var regionPopup = new mapboxgl.Popup({anchor:'right', className: 'biopamaPopup', offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(popup)
                    .addTo(map);
            } 
            
            if (popUpContents.countryCode.length){
                var areaCheck = "areaInactive";
                if (selSettings.iso2 == popUpContents.countryCode)areaCheck = "areaActive";
                var popup = popUpSection(areaCheck, popUpContents.country, popUpContents.countryCode, "country");
                var countryPopup = new mapboxgl.Popup({anchor:'bottom', className: 'biopamaPopup', offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(popup)
                    .addTo(map);
            } 
    
            var numberOfPas = "singlePA";
            var paPopupContent = '';
            if (popUpContents.PAs.length){
                if (popUpContents.PAs.length == 1){
                    checkPA(popUpContents.WDPAIDs[0], 0)
                } else {
                    numberOfPas = "multiPAs"
                    for (var key in popUpContents.PAs) {
                        checkPA(popUpContents.WDPAIDs[key], key)
                    }
                }
                var paPopup = new mapboxgl.Popup({anchor:'left', className: 'biopamaPopup '+numberOfPas, offset: 40, closeButton: false})
                    .setLngLat(e.lngLat)
                    .setHTML(paPopupContent)
                    .addTo(map);
            }
            function checkPA(paID, paKey){
                var areaCheck = "areaInactive";
                if (paID == selSettings.WDPAID)areaCheck = "areaActive";
                paPopupContent = paPopupContent + popUpSection(areaCheck, popUpContents.PAs[paKey], popUpContents.WDPAIDs[paKey], "pa");
            }

            if (closePopupIcon !== undefined){
                closePopupIcon.remove();
            }

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]:not(.tooltipified)'))
			var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
				$(tooltipTriggerEl).addClass("tooltipified");
				return new bootstrap.Tooltip(tooltipTriggerEl)
			})

            if ((popUpContents.regionID.length) || (popUpContents.countryCode.length) || (popUpContents.PAs.length)){
                // create a HTML element for the centre
                var el = document.createElement('div');
                el.className = 'mapCloseMarker';
                el.innerHTML = '<i class="fas fa-times"></i>';
                
                closePopupIcon = new mapboxgl.Marker(el).setLngLat(e.lngLat).addTo(map);	

                $( "div.mapCloseMarker" ).click(function(e) {
                    e.stopPropagation();
                    closeMapCards();
                });
            } 
 
            $( "div.region div.cardSelect" ).click(function(e) {
                selSettings.regionName = popUpContents.region;
                selSettings.regionID = popUpContents.regionID;
                updateRegion(selSettings.regionID);
                closeMapCards();
            });
            $( "div.country div.cardSelect" ).click(function(e) {
                selSettings.iso2 = popUpContents.countryCode;
                var region = getMapLayerInfo(e.point, "regionMask");
                if (typeof region !== 'undefined') {
                    selSettings.regionID = popUpContents.regionID;
                }
                countryChanged = 1;
                updateCountry();
                closeMapCards();
            });
            $( "div.pa div.cardSelect" ).click(function(e) {
                selSettings.WDPAID = $(this).attr('id');
                selSettings.paName = $(this).closest(".card").find(".area-subheading").text().trim();
                selSettings.iso2 = popUpContents.countryCode;
                jQuery().updateSectedCountry();
                updateCountry('iso2', false);
                updatePa();
                closeMapCards();
            });
            $( "div.card.region" ).hover(function(e) {
                map.setFilter('regionHover', ['==', 'Group', popUpContents.region]);		
                map.setLayoutProperty("regionHover", 'visibility', 'visible');	
            });
            $( "div.card.country" ).hover(function(e) {
                map.setFilter('countryHover', ['==', 'iso3', popUpContents.country3Code]);		
                map.setLayoutProperty("countryHover", 'visibility', 'visible');	
            });
            $( "div.card.pa" ).hover(function(e) {
                var wdpaID = parseInt($(this).attr('id'), 10);
                map.setFilter('wdpaAcpHover', ['==', 'wdpaid', wdpaID]);		
                map.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');	
            });
            function closeMapCards(){
                $('.tooltipified').tooltip('dispose');
                if (regionPopup !== undefined)regionPopup.remove();
                if (countryPopup !== undefined)countryPopup.remove();
                if (paPopup !== undefined)paPopup.remove();
                closePopupIcon.remove();
            }
            function popUpSection(activeStatus, cardTitle, spatialID, spatialScope){
                var cardArea = spatialScope;
                var cardURL = theseOptions[spatialScope].url;
                if (spatialScope == 'pa'){
                    cardArea = 'Protected Areas';
                }
                if (theseOptions[spatialScope].url !== '#'){
                    cardURL = cardURL + spatialID;
                }
                var tempCard = '<div class="card mapPopupCard text-black '+ activeStatus +' ' + spatialScope + '" id="'+spatialID+'">'+
                    '<div class="card-header card-header-'+ spatialScope +'">'+ cardArea + '</div>'+
                    '<div class="card-body cardSelect" id="'+spatialID+'" >'+
                        '<div class="row">'+
                            '<div class="col-12" data-bs-toggle="tooltip" data-bs-placement="top" title="Select this '+cardArea+'">'+
                                '<h6 class="area-subheading">' + cardTitle + '</h6>'+
                            '</div>';
                        tempCard += '</div>'+
                    '</div>'+
                '</div>';
                if (theseOptions[spatialScope].url !== '#'){
                    tempCard += '<div class="col-12 area-icons w-100 d-flex justify-content-center">'+
                        '<a href="' + cardURL + '" class="cardIcon" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Goto this '+cardArea+' page"><i class="fas fa-arrow-right"></i></a>'+
                    '</div>';
                }
                return tempCard;
            }
        }
        
        function regionCheck(e){
            var region = getMapLayerInfo(e.point, "regionMask");
            if (typeof region !== 'undefined') {
                popUpContents.region = region[0].properties.Group;
                switch(popUpContents.region) {
                    case "Eastern Africa":
                        popUpContents.regionID = "eastern_africa";
                        break;
                    case "Central Africa":
                        popUpContents.regionID = "central_africa";
                        break;
                    case "Western Africa":
                        popUpContents.regionID = "western_africa";
                        break;
                    case "Southern Africa":
                        popUpContents.regionID = "southern_africa";
                        break;
                    default:
                        popUpContents.regionID = popUpContents.region;
                        break;
                    }
            } 
            return;
        }

        function countryCheck(e){
            var country = getMapLayerInfo(e.point, "countryMask");
            if (typeof country !== 'undefined') {
                var thisCountry = country[0].properties;
                if (thisCountry.hasOwnProperty('name_iso31')){
                    popUpContents.country = thisCountry.name_iso31;
                } else {
                    popUpContents.country = thisCountry.country_name;
                }
                popUpContents.countryCode = thisCountry.iso2; //For the Country Page
                popUpContents.country3Code = thisCountry.iso3; //For PA feature matching
            }
            return;
        }
    
        function paCheck(e){
            var PAs = getMapLayerInfo(e.point, "wdpaAcpMask");
            if (typeof PAs !== 'undefined') {
                for (var key in PAs) {
                    popUpContents.PAs.push(PAs[key].properties.name);
                    popUpContents.WDPAIDs.push(PAs[key].properties.wdpaid);
                    popUpContents.paIUCNCats.push(PAs[key].properties.iucn_cat);
                    popUpContents.paSize.push(PAs[key].properties.gis_area);
                    if (PAs[key].properties.wdpaid == selSettings.WDPAID){
                        
                    }
                }		
            }
            return;
        }

    }

    //returns the first country object
    function getMapLayerInfo(point, mapLayer){
        var thisLayer = thisMap.getLayer(mapLayer);
        
        if(typeof thisLayer !== 'undefined') {
            var feature = thisMap.queryRenderedFeatures(point, {
                layers:[mapLayer],
            });
            //as long as we have something in the feature query 
            if (typeof feature[0] !== 'undefined'){
                return feature;
            }
        } else {
            return;
        }
    }

    
})(jQuery);

function updateRegion(region = selSettings.regionID, countryUpdate = true, zoomTo = true) {
	regionChanged = 0;
	if((selSettings.iso2 !== null) && (countryUpdate === true)){
		removeCountry();
	}
	if (zoomTo) {	
		jQuery().mapZoomToRegion(thisMap, region); 	
	}

    if(window.document.querySelector(".regional-tab.active")){
        var matches = window.document.querySelectorAll(".indicator-card");
        matches.forEach(function(element) {
            var node_id = element.querySelector(".node_id");
            getRestResults(node_id.innerText);
        });
    }
	thisMap.setLayoutProperty("regionSelected", 'visibility', 'visible');
	thisMap.setFilter('regionSelected', ['==', 'Group', selSettings.regionName]);
	//updateAddress();
	//updateBreadRegion();
	jQuery('.mapboxgl-ctrl-z-region').show();
	
}

function updateCountry(isoProperty = 'iso2', zoomTo = true, clearPA = true) {
	//If we are moving to a new country the currently selected PA can't follow us there, so we remove it
	//if((clearPA === true) && (selSettings.WDPAID > 0)){ removePA(); }
	jQuery().updateSectedCountry(isoProperty);

	if (zoomTo) {
		jQuery().mapZoomToCountryIso2(thisMap, selSettings.iso2);
		var currentPath = window.location.pathname;
		if(!currentPath.includes("/country/") && !currentPath.includes("/pa/")){
			updateAddress();
		}
	}

    if(window.document.querySelector("#country-selector")){
        var matches = window.document.querySelectorAll(".biopama-country-selector");
        matches.forEach(function(element) {
            // element.value = selSettings.iso2;
            // element.trigger("chosen:updated");
            jQuery(".biopama-country-selector").val(selSettings.iso2).trigger("chosen:updated");
            jQuery(".biopama-country-selector").trigger("change");
        });
    }

    if(window.document.querySelector(".national-tab.active")){
        var matches = window.document.querySelectorAll(".indicator-card");
        matches.forEach(function(element) {
            var node_id = element.querySelector(".node_id");
            getRestResults(node_id.innerText);
        });
    }
    if (thisMap.getLayer("wdpaAcpPolyLabels")) {
        thisMap.setFilter("wdpaAcpPolyLabels", [ "all", ["in", "Point", 0], ['in', 'ISO3', selSettings.iso3] ]);
    }
    if (thisMap.getLayer("wdpaAcpPointLabels")) {
        thisMap.setFilter("wdpaAcpPointLabels", [ "all", ["in", "Point", 1], ['in', 'ISO3', selSettings.iso3] ]);
    }
    if (thisMap.getLayer("wdpaAcpFillHighlighted")) {
        thisMap.setFilter('wdpaAcpFillHighlighted', ['==', 'iso3', selSettings.iso3]);
        thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');
    }
    if (thisMap.getLayer("wdpaAcpFill")) {
        thisMap.setFilter('wdpaAcpFill', ['!=', 'iso3', selSettings.iso3]);
    }
    if (thisMap.getLayer("wdpaAcpPointLabels")) {
        thisMap.setFilter("wdpaAcpPointLabels", [ "all", ["in", "Point", 1], ['in', 'ISO3', selSettings.iso3] ]);
    }

    if (selSettings.regionName !== null){
        thisMap.setFilter('countryFill', [ "all", ['==', 'Group', selSettings.regionName], ['!=', 'iso3', selSettings.iso3] ]);
        thisMap.setLayoutProperty("countryFill", 'visibility', 'visible');
    } 
	
	thisMap.setLayoutProperty("countrySelected", 'visibility', 'visible');
	thisMap.setFilter('countrySelected', ['==', 'iso3', selSettings.iso3]);
	
	jQuery('.mapboxgl-ctrl-layer-fill').show();
	jQuery('.mapboxgl-ctrl-z-country').show();
}
function updatePa() {
	paChanged = 0;
	thisMap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
	thisMap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', selSettings.WDPAID]);
	thisMap.setPaintProperty('wdpaAcpSelected', 'line-width', 10);
	setTimeout(function(){
		thisMap.setPaintProperty('wdpaAcpSelected', 'line-width', 2);
	}, 300);

    if(window.document.querySelector(".local-tab.active")){
        var matches = window.document.querySelectorAll(".indicator-card");
        matches.forEach(function(element) {
            var node_id = element.querySelector(".node_id");
            getRestResults(node_id.innerText);
        });
    }

	jQuery('.mapboxgl-ctrl-z-pa').show();
	//udateAddress();
	jQuery().mapZoomToPA(thisMap, selSettings.WDPAID);
}
