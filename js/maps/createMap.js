(function($){
    
	$.fn.createMap = function(mapContainer, mapOptions = {}){
        var mapTheme = biopamaGlobal.map.baseLayerStyle;
        var theme = jQuery.jStorage.get("theme", "not set");
        if (theme == "light"){
            mapTheme = 'light-v10';
        } 
        mapboxgl.accessToken = biopamaGlobal.map.mapboxAccessToken;
        var map = new mapboxgl.Map({
            container: mapContainer,
            style: mapTheme, //Andrews default new RIS v2 style based on North Star
            projection: 'mercator',
            // attributionControl: true,
            // renderWorldCopies: true,
            center: mapOptions.center || biopamaGlobal.map.center,
            zoom: mapOptions.zoom || biopamaGlobal.map.zoom,
            //minZoom: mapOptions.minZoom || 1.4,
            //maxZoom: mapOptions.maxZoom || 20,
        }).addControl(new mapboxgl.AttributionControl({
            customAttribution: biopamaGlobal.map.attribution,
            compact: true
        }));

        thisMap = map;
	
        map.addControl(new mapboxgl.ScaleControl({
            maxWidth: 150,
            unit: 'metric'
        }));

        $().addMapLayer(map, "base");

        return map;

    }
    $.fn.addMapControls = function(map, control){
        //var buttonClasses = "text-white p-0 align-self-center btn btn-warning map-ctrl-button";
        switch(control){
        case "fullScreen":
            map.addControl(new mapboxgl.FullscreenControl());
            break;
        case "navigation":
            map.addControl(new mapboxgl.NavigationControl());
            break;
        case "satelliteToggle":
            class mapCtToolsControl {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group';
                    this._container.innerHTML = "<button class='mapboxgl-ctrl-icon mapboxgl-ctrl-sat' title='Toggle Satellite base layer' id='satellite-layer' type='button' data-bs-toggle='tooltip' aria-label='Toggle Satellite base layer' data-bs-original-title='Toggle Satellite base layer'><i class='fa-solid fa-satellite'></i></button>";
                    return this._container;
                }
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            }
            mapCtTools = new mapCtToolsControl();
            map.addControl(mapCtTools, 'top-right');

            $('#satellite-layer').bind("click", function(){
                if ($( "#satellite-layer" ).hasClass( "sat-on" )) {
                    $('#satellite-layer').removeClass( "sat-on" );
                    map.setLayoutProperty('satellite', 'visibility', 'none');
                } else {
                    $('#satellite-layer').addClass( "sat-on" );
                    map.setLayoutProperty('satellite', 'visibility', 'visible');
                }
            });

            break;
        case "zoom":
            class mapZoomControl {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group map-zoom-group';
                    this._container.innerHTML = "<button class='mapboxgl-ctrl-icon mapboxgl-ctrl-z-global' type='button' title='Zoom to global extent' data-bs-toggle='tooltip' aria-label='Zoom to full extent' data-bs-original-title='Zoom to full extent'> </button>"+
                "<button class='mapboxgl-ctrl-icon mapboxgl-ctrl-z-region' type='button' title='Zoom to regional extent' style='display: none;' data-bs-toggle='tooltip' aria-label='Zoom to regional extent' data-bs-original-title='Zoom to regional extent'> </button>"+
                "<button class='mapboxgl-ctrl-icon mapboxgl-ctrl-z-country' type='button' title='Zoom to country extent' style='display: none;' data-bs-toggle='tooltip' aria-label='Zoom to country extent' data-bs-original-title='Zoom to country extent'> </button>"+
                "<button class='mapboxgl-ctrl-icon mapboxgl-ctrl-z-pa' type='button' title='Zoom to protected area extent' style='display: none;' data-bs-toggle='tooltip' aria-label='Zoom to protected area extent' data-bs-original-title='Zoom to protected area extent'> </button>";
                    return this._container;
                }
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            }
            mapZoom = new mapZoomControl();
            map.addControl(mapZoom, 'top-right');

            $('.mapboxgl-ctrl-z-global').bind("click", function(){
                map.flyTo({
                    center: biopamaGlobal.map.center,
                    zoom: biopamaGlobal.map.zoom,
                });
            });
            $('.mapboxgl-ctrl-z-region').bind("click", function(){
                //zoomToRegion(selSettings.regionID);
                $().mapZoomToRegion(map, selSettings.regionID);
            });
            $('.mapboxgl-ctrl-z-country').bind("click", function(){
                $().mapZoomToCountryIso2(map, selSettings.iso2);
            });
            $('.mapboxgl-ctrl-z-pa').bind("click", function(){
                //zoomToPA(selSettings.WDPAID);
                $().mapZoomToPA(map, selSettings.WDPAID);
            });
            break;

        case "projection":
            class mapProjectionControl {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group map-projection-group';
                    this._container.innerHTML = "<button class='mapboxgl-ctrl-icon mapboxgl-projection-mercator' type='button' title='Set projection to Mercator' data-bs-toggle='tooltip' aria-label='Set projection to Mercator' data-bs-original-title='Set projection to Mercator'><i class='fa-solid fa-minus'></i></button>"+
                "<button class='mapboxgl-ctrl-icon mapboxgl-projection-globe' type='button' title='Set projection to Globe' data-bs-toggle='tooltip' aria-label='Set projection to Globe' data-bs-original-title='Set projection to Globe'><i class='fa-solid fa-earth-africa'></i></button>";
                    return this._container;
                }
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            }
            mapProjection = new mapProjectionControl();
            map.addControl(mapProjection, 'top-right');

            $('.mapboxgl-projection-mercator').bind("click", function(){
                map.setProjection({name: "mercator"});
            });
            $('.mapboxgl-projection-globe').bind("click", function(){
                map.setProjection({name: "globe"});
            });
            break;

        case "hoveredAreaLabelBlock":
            class mapHoveredAreaLabelBlock {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl map-country-pa-info-wrapper text-dark';
                    this._container.innerHTML = "<div id='map-region-info'></div><div id='map-country-info'></div><div id='map-pa-info'></div><div id='map-indicator-info'></div>";
                    return this._container;
                }
        
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            } 
            hoveredAreaLabelBlock = new mapHoveredAreaLabelBlock();
            map.addControl(hoveredAreaLabelBlock, 'bottom-right');	
            break;

        case "paPolyFillControl":
            class paFillControl {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group mapboxgl-ctrl-layer-fill';
                    this._container.innerHTML = "<button class='mapboxgl-ctrl-icon' type='button' id='layer-fill-toggle' title='Toggle fill for selected polygons'><i class='fas fa-fill-drip'></i></button>";
                    return this._container;
                }
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            }
            paFill = new paFillControl();
            map.addControl(paFill, 'top-right');

            $('#layer-fill-toggle').bind("click", function(){
                var currentLayers = map.style._layers;
                if ($( "#layer-fill-toggle" ).hasClass( "fill-off" )) {
                    $('#layer-fill-toggle').removeClass( "fill-off" );
                    //'fill-color': 'rgba(255, 152, 0, 0.5)',
                    if (map.getLayer("wdpaAcpFillHighlighted")) map.setPaintProperty("wdpaAcpFillHighlighted", 'fill-color', 'rgba(255, 152, 0, 0.5)');
                    for (var key in currentLayers) { 
                        if (currentLayers[key].id.indexOf('1nd1l4y3r') != -1) {
                            map.setPaintProperty(currentLayers[key].id, 'fill-color', indictorGlobalSettings[nodeID].paintProp);
                        } 
                    }
                    
                } else {
                    $('#layer-fill-toggle').addClass( "fill-off" );
                    if (map.getLayer("wdpaAcpFillHighlighted")) map.setPaintProperty("wdpaAcpFillHighlighted", 'fill-color', 'rgba(0, 0, 0, 0)');
                    for (var key in currentLayers) { 
                        if (currentLayers[key].id.indexOf('1nd1l4y3r') != -1) {
                            map.setPaintProperty(currentLayers[key].id, 'fill-color', 'rgba(0, 0, 0, 0)');
                        } 
                    }
                }
            });

            break;

        case "loaderControl":
            class mapLoaderControl {
                onAdd(map) {
                    this._map = map;
                    this._container = document.createElement('div');
                    this._container.className = 'mapboxgl-ctrl ajax-loader ajax-load';
                    this._container.innerHTML = "<div id='map-loader-wrapper'>"+
                "<div id='map-loader'></div>"+
                "</div>";
                    return this._container;
                }
                onRemove() {
                    this._container.parentNode.removeChild(this._container);
                    this._map = undefined;
                }
            }
            mapLoader = new mapLoaderControl();
            map.addControl(mapLoader, 'top-left');	

            $('.mapboxgl-ctrl.ajax-loader').toggle(false);
            break;

        default:
            console.log("the control " + loaderControl + " failed to load for some reason. Check it out.");
        }
    }
})(jQuery);