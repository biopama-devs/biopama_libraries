(function($){
	$.fn.createXYAxisChart = function(chartSelector = null, chartData = null, report = false, nodeID = "none"){ 

	// if (indictorGlobalSettings[nodeID].firstChartRun === undefined){
	// 	indictorGlobalSettings[nodeID].firstChartRun = 1;
	// }
	// if (indictorGlobalSettings[nodeID].firstChartRun){
	// 	indictorGlobalSettings[nodeID].firstChartRun = 0;
		
	// }

	//merging the CTT chart with global function here. To be improved in CTT module
	if (chartData === null){
		chartData = (chartSettings.data).slice();
	}

/**
 * 
 * Code that is needed for the global dataset module
 * todo - remove it?
 * 
 */
		if (report){
			thisTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[chartSelector+'Chart'] = chartData;
			activeIndicators[chartSelector+'Chart'].type = 'chart';
			activeIndicators[chartSelector+'Chart'].chartType = 'XYAxis';
		}
/**
 * 
 * end code needed for global dataset module
 * 
 */

 		let $chart = $().selectorCheck(chartSelector).get(0); 
	
		var chartTitle = null;
		var thisToolbox = null;
		var thisTooltip = null;
		var chartDataZoom = null;

		if(chartData.title){
			chartTitle = {
				text: chartData.title,
				show: true,
			};
		}

		if(!chartData.toolbox){
			thisToolbox = {
				show: true,
				right: 20,
				top: -8,
				feature: {
					saveAsImage: {
						title: 'Save',
					},
					myTable: {
						show: true,
						title: 'Tabular View',
						icon: 'path://M5.42,0h112.04c2.98,0,5.42,2.44,5.42,5.42V97.1c0,2.98-2.44,5.42-5.42,5.42H5.42c-2.98,0-5.42-2.44-5.42-5.42 V5.42C0,2.44,2.44,0,5.42,0L5.42,0z M8.48,23.58H38.1c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48H8.48 c-0.81,0-1.48-0.67-1.48-1.48v-9.76C6.99,24.25,7.66,23.58,8.48,23.58L8.48,23.58z M84.78,82.35h29.63c0.82,0,1.48,0.67,1.48,1.48 v9.76c0,0.81-0.67,1.48-1.48,1.48H84.78c-0.81,0-1.48-0.67-1.48-1.48v-9.76C83.29,83.02,83.96,82.35,84.78,82.35L84.78,82.35z M46.8,82.35h29.28c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48H46.8c-0.81,0-1.48-0.67-1.48-1.48v-9.76 C45.31,83.02,45.98,82.35,46.8,82.35L46.8,82.35z M8.48,82.35H38.1c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48 H8.48c-0.81,0-1.48-0.67-1.48-1.48v-9.76C6.99,83.02,7.66,82.35,8.48,82.35L8.48,82.35z M84.78,62.76h29.63 c0.82,0,1.48,0.67,1.48,1.48V74c0,0.81-0.67,1.48-1.48,1.48H84.78c-0.81,0-1.48-0.67-1.48-1.48v-9.76 C83.29,63.43,83.96,62.76,84.78,62.76L84.78,62.76z M46.8,62.76h29.28c0.82,0,1.48,0.67,1.48,1.48V74c0,0.81-0.67,1.48-1.48,1.48 H46.8c-0.81,0-1.48-0.67-1.48-1.48v-9.76C45.31,63.43,45.98,62.76,46.8,62.76L46.8,62.76z M8.48,62.76H38.1 c0.82,0,1.48,0.67,1.48,1.48V74c0,0.81-0.67,1.48-1.48,1.48H8.48c-0.81,0-1.48-0.67-1.48-1.48v-9.76 C6.99,63.43,7.66,62.76,8.48,62.76L8.48,62.76z M84.78,43.17h29.63c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48 H84.78c-0.81,0-1.48-0.67-1.48-1.48v-9.76C83.29,43.84,83.96,43.17,84.78,43.17L84.78,43.17z M46.8,43.17h29.28 c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48H46.8c-0.81,0-1.48-0.67-1.48-1.48v-9.76 C45.31,43.84,45.98,43.17,46.8,43.17L46.8,43.17z M8.48,43.17H38.1c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48 H8.48c-0.81,0-1.48-0.67-1.48-1.48v-9.76C6.99,43.84,7.66,43.17,8.48,43.17L8.48,43.17z M84.78,23.58h29.63 c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48H84.78c-0.81,0-1.48-0.67-1.48-1.48v-9.76 C83.29,24.25,83.96,23.58,84.78,23.58L84.78,23.58z M46.8,23.58h29.28c0.82,0,1.48,0.67,1.48,1.48v9.76c0,0.81-0.67,1.48-1.48,1.48 H46.8c-0.81,0-1.48-0.67-1.48-1.48v-9.76C45.31,24.25,45.98,23.58,46.8,23.58L46.8,23.58z',
						onclick: function (chart, dom){
							jQuery().chartTable(chart, dom); 
						}
					},
				}
			};
		}

		if(!chartData.tooltip){
			thisTooltip = {
				trigger: 'axis'
			};
			//thisTooltip = biopamaGlobal.chart.tooltip;
		}

		if(chartData.dataZoom){
			chartDataZoom = [
				{
					type: 'slider',
					xAxisIndex: [0],
					filterMode: 'filter'
				}
			];
		}

		if (report){
			chartTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		if (chartData.xAxis.nameLocation == undefined){
			chartData.xAxis['nameLocation'] = "middle";
		}
		if (chartData.xAxis.nameGap == undefined){
			chartData.xAxis['nameGap'] = 30;
		}
		if (chartData.yAxis.nameLocation == undefined){
			chartData.yAxis['nameLocation'] = "middle";
		}
		if (chartData.yAxis.nameGap == undefined){
			chartData.yAxis['nameGap'] = 60;
		}

		chartData.yAxis['nameRotate'] = 90;
		
		indicatorCharts[nodeID] = echarts.init($.fn.getChartDomElement($chart));
		var option = { 
			title: chartTitle, 
			legend: chartData.legend,
			xAxis: chartData.xAxis,
			tooltip: chartData.tooltip || thisTooltip,
			toolbox: chartData.toolbox || thisToolbox,
			grid: {containLabel: true, left: 0, right: 0},
			yAxis: chartData.yAxis,
			series: chartData.series,
			dataZoom: chartDataZoom,
			animationDuration: 600,
		};
		
		if (chartData.colors){
			chartData.color = chartData.colors;
		} 
		if (chartData.color) {
			var colors = chartData.color;
			if (typeof colors  === 'string'){
				if (colors !== "ignore"){
					option.color = biopamaGlobal.chart.colors[chartData.color];
				} else {
					option.color = [];
				}
			} else if (Array.isArray(colors)){
				option.color = chartData.color;
			}
		} else {
			option.color = biopamaGlobal.chart.colors.default;
		}
		
		if(option.title){
			option.title.textStyle = {
				color: biopamaGlobal.chart.colors.text
			}
		}

		if (option.legend !== undefined){
			if(option.legend.itemStyle == undefined){
				option.legend["textStyle"] = {
					color: biopamaGlobal.chart.colors.text
				  }
			}
		}

		if(option.xAxis.axisLine == undefined){
			option.xAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		}

		if(option.xAxis.axisLabel == undefined){
			option.xAxis['axisLabel'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		}

		if(option.yAxis.axisLine == undefined){
			option.yAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		}

		if(option.yAxis.axisLabel == undefined){
			option.yAxis['axisLabel'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
		}
		
		if ($(chartSelector).length >= 1){
			$().removeBiopamaLoader(chartSelector);
		}

		indicatorCharts[nodeID].setOption(option);

		return indicatorCharts[nodeID];
	}
})(jQuery);
