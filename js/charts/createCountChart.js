(function($){
	$.fn.createCountChart = function(chartSelector, chartData){

		var littleNumber = '';
		var bigNumberSuffix = chartData.bigNumberSuffix || '';
		var littleNumberSuffix = chartData.littleNumberSuffix || '';
		var countChartPrefix = chartData.countChartPrefix || '';
		var countChartSuffix = chartData.countChartSuffix || '';

		if(chartData.littleNumber){
			littleNumber = '<span class="count-chart-little-number">' + chartData.littleNumber + littleNumberSuffix + '</span>';
		}

		var countChart = '<div class="biopama-stats-item">'+
			'<div class="biopama-stats-item-title fs-4">' +
				chartData.title +
			'</div>'+
			'<div class="biopama-stats-item-content fs-1 biopama-stats-item-text 111">' +
				'<span>' + countChartPrefix + chartData.bigNumber + bigNumberSuffix + littleNumber + countChartSuffix + '</span>' +
			'</div>' + 
		'</div>';

		$().removeBiopamaLoader(chartSelector);

		$(chartSelector).html(countChart);
	}
})(jQuery);

