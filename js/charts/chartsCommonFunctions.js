(function($){
    $.fn.getChartDomElement = function(elementOrSelector){
        let element = elementOrSelector;
        if(typeof elementOrSelector == 'string'){
            element = $(elementOrSelector).get(0);
        }
        return element;
    }

    $.fn.chartTable = function(chart, dom){
        var chartData = $().chartCSV(chart, dom, false);
        //console.log(chart);
        //$( ".wrapper-data-table" ).remove(); //remove any other chart tables that might exsist.
        //document.getElementById(dom.getDom().id).firstChild.style.display = 'visible';
        var thisChart = document.getElementById(dom.getDom().id);
        // var wrapper = document.createElement('div');
        // thisChart.parentNode.insertBefore(wrapper, thisChart);
        // wrapper.appendChild(thisChart);
        //new_html = "<div id='slidesInner'>" + org_html + "</div>";
        //$( '<div class="wrapper-data-table p-2 text-bg-dark"><table class="data-table display" id="data-table" width="100%"></table></div>' ).insertBefore( chartID );
        var thisTable = document.createElement('div');
        thisTable.id = "table-wrapper-"+dom.getDom().id;
        thisTable.classList.add("wrapper-data-table", "p-2", "text-bg-dark");
        thisTable.innerHTML = '<table class="data-table display" id="data-table-'+dom.getDom().id+'" width="100%"></table>';
        thisChart.appendChild(thisTable)
        //$( chartID ).hide();
        document.getElementById(dom.getDom().id).firstChild.style.display = 'none';

        var columnData = [];

        const headers = Object.keys(chartData[0]);

        headers.forEach((item) => {
            columnData.push({
                title: item, data: item
            });
        });

        var columnSettings = [{}];

        var tableData = {
            //title: indicatorTitle,
            columns: columnData,
            columnDefs: columnSettings,
            data: chartData,
            //attribution: dopaAttribution,
            isComplex: true
        }

        $().createDataTable('#data-table-'+dom.getDom().id, tableData, false, true); 
    }

    $.fn.chartCSV = function(chart, dom, chartDownload = true){
        var chartOption = chart.option;
        var series;
        var firstSeries;
        var chartType;
        if (chartOption.hasOwnProperty("series")){
            series = chart.option.series;
            firstSeries = chart.option.series[0];
            chartType = chart.option.series[0].type;
        }
        var dataPoints = firstSeries.data; //an array of objects with the data from the first series in the chart

        const chartDataArray = [];

        for (const [dataIndex, element] of dataPoints.entries()) {
            addCSVRow(element, dataIndex)
        }

        function addCSVRow(element, dataIndex){
            const chartdata = {};
            if (chartType !== "pie") {
                if(firstSeries.hasOwnProperty("countryLabels")){
                    if (typeof firstSeries["countryLabels"][dataIndex] !== "undefined"){
                        chartdata["Country Name"] = firstSeries["countryLabels"][dataIndex];
                    }
                }
                if(firstSeries.hasOwnProperty("countryISO2")){
                    if (typeof firstSeries["countryISO2"][dataIndex] !== "undefined"){
                        chartdata["ISO2"] = firstSeries["countryISO2"][dataIndex];
                    }
                }
                if(chart.option.hasOwnProperty("xAxis")){
                    if (typeof chart.option.xAxis[0].data !== "undefined"){
                        chartdata["Label"] = chart.option.xAxis[0].data[dataIndex];
                    }
                }
    
                if (element == null) {
                    element = "null";
                }
    
                if (typeof element === 'object') {
                    chartdata[firstSeries['name'].trim()] = element.value !== "null" ? jQuery().roundDown(element.value) : "null";
                } else {
                    if(firstSeries.hasOwnProperty("name")){
                        chartdata[firstSeries['name'].trim()] = element !== "null" ? jQuery().roundDown(element) : "null";
                    } else {
                        chartdata["value"] = element !== "null" ? jQuery().roundDown(element) : "null";
                    }
                }

                //series.forEach(addOtherSeriesData(element, seriesIndex));
                for (const [seriesIndex, element] of series.entries()) {
                    addOtherSeriesData(element, seriesIndex, dataIndex, chartdata)
                }
            } else {
                var thisName = dataPoints[dataIndex]['name'].trim();
                var thisValue = dataPoints[dataIndex]['value'];
                thisValue = parseFloat(thisValue, 10);
                chartdata["Label"] = thisName;
                chartdata["Value"] = thisValue !== "null" ? jQuery().roundDown(thisValue) : "null";
            }
            
            chartDataArray.push(chartdata);
        }

        function addOtherSeriesData(element, seriesIndex, dataIndex, chartdata){
            if(seriesIndex == 0){
                return; //skip the first series, we did that already.
            } else {
                var data = jQuery().roundDown(chart.option.series[seriesIndex].data[dataIndex]);
                if (data == null) {
                    data = "null";
                }
                chartdata[chart.option.series[seriesIndex]['name'].trim()] = data !== "null" ? data : "null";
            } 
        }

        if (chartDownload == true){
            const csvdata = csvmaker(chartDataArray);
            download(csvdata);
        } else {
            return chartDataArray;
        }
        
    }

    $.fn.roundDown = function(number = 0, decimals = 3){
        var dataVal = 0;
        var numS = number.toString();
        var decPos = numS.indexOf('.');
        if (decPos >= 0){
            dataVal = parseFloat(number, 10).toFixed(decimals)
        } else {
            dataVal = parseFloat(number, 10);
        }
        return dataVal;
    }

    const csvmaker = function (data) {
  
        // Empty array for storing the values
        csvRows = [];
      
        // Headers is basically a keys of an object which 
        // is id, name, and profession
        const headers = Object.keys(data[0]);
      
        // As for making csv format, headers must be
        // separated by comma and pushing it into array
        csvRows.push(headers.join(','));
      
        // Pushing Object values into the array with
        // comma separation
      
        // Looping through the data values and make
        // sure to align values with respect to headers
        for (const row of data) {
            const values = headers.map(e => {
                return row[e]
            })
            csvRows.push(values.join(','))
        }
      
        // const values = Object.values(data).join(',');
        // csvRows.push(values)
      
        // returning the array joining with new line 
        return csvRows.join('\n')
    }

    const download = function (data) {
  
        // Creating a Blob for having a csv file format 
        // and passing the data with type
        const blob = new Blob([data], { type: 'text/csv' });
      
        // Creating an object for downloading url
        const url = window.URL.createObjectURL(blob)
      
        // Creating an anchor(a) tag of HTML
        const a = document.createElement('a')
      
        // Passing the blob downloading url 
        a.setAttribute('href', url)
      
        // Setting the anchor tag attribute for downloading
        // and passing the download file name
        a.setAttribute('download', 'download.csv');
      
        // Performing a download with click
        a.click()
    }
})(jQuery);
