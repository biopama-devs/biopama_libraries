(function($){
	$.fn.createNoAxisChart = function(chartSelector, chartData, report = false, nodeID = "none"){

		let $chart = $().selectorCheck(chartSelector).get(0); 

		var chartTitle = null;
		var chartToolbox = null;
		var chartTooltip = null;
		var chartTitleTextAlign = null;
		
		if (report){
			chartTitle = {};
			chartTooltip = {};
			chartToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[chartSelector+'Chart'] = chartData;
			activeIndicators[chartSelector+'Chart'].type = 'chart';
			activeIndicators[chartSelector+'Chart'].chartType = 'NoAxis';
		}

		if(chartData.titleAlign){
			chartTitleTextAlign = chartData.titleAlign;
		}

		if(chartData.title){
			chartTitle = {
				text: chartData.title,
				show: true,
				left: chartTitleTextAlign
			};
		}

		if(!chartData.toolbox){
			chartToolbox = biopamaGlobal.chart.toolbox;
		}

		if(!chartData.tooltip){
			chartTooltip = {
				trigger: 'item'
			};
			//chartTooltip = biopamaGlobal.chart.tooltip;
		}


		//pie charts are VERY similar to line and bar charts, but all the config data is in the series, so we need a separate function 
		indicatorCharts[nodeID] = echarts.init($.fn.getChartDomElement($chart));
		var option = {
			title: chartTitle,
			toolbox: chartToolbox,
			grid: {containLabel: true, left: 0, right: 0}, 
			tooltip: chartData.tooltip || chartTooltip, 
  			legend: chartData.legend,
			series: chartData.series,
			animationDuration: 600
		};

		if (option.legend !== undefined){
			if(option.legend.itemStyle == undefined){
				option.legend["textStyle"] = {
					color: biopamaGlobal.chart.colors.text
				  }
			}
		}

		if(chartData.radar){
			option.radar = chartData.radar;
		}

		if(chartData.angleAxis){
			option.angleAxis = chartData.angleAxis;
		}

		if(chartData.radiusAxis){
			option.radiusAxis = chartData.radiusAxis;
		}

		if(chartData.polar){
			option.polar = chartData.polar;
		}

		if(chartData.singleVal){
			option.series[0]['labelLine'] = {show: false};
			option.series[0]['selectedMode'] = 'single';
			option.series[0]['avoidLabelOverlap'] = false;
			option.series[0]['label'] = {
				position: 'center',
				fontSize: '20',
				//fontWeight: 'bold',
				color: biopamaGlobal.chart.colors.text,
			};
			option.series[0].data[0]['label'] = {show: true};
			option.series[0].data[1]['emphasis'] = { disabled: true};
			option.series[0].data[1]['label'] = {show: false};
		}

		if(option.title){
			option.title.textStyle = {
				color: biopamaGlobal.chart.colors.text
			}
		}

		if (chartData.colors){
			var colors = chartData.colors;
			if (typeof colors  === 'string'){
				option.color = biopamaGlobal.chart.colors[chartData.colors];
			} else if (Array.isArray(colors)){
				option.color = chartData.colors;
			}
		} else {
			option.color = biopamaGlobal.chart.colors.default;
		}

		if (chartData.dataset){
			option.dataset = chartData.dataset;
		} 
		
		if ($(chartSelector).length >= 1){
			$().removeBiopamaLoader(chartSelector);
		}

		indicatorCharts[nodeID].setOption(option);

		return indicatorCharts[nodeID];
	}
})(jQuery);
