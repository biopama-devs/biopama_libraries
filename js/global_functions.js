/**
 * These functions are namespaced by jQuery, it helps make sure the names don't conflict with other peoples code since we work with opensource code.
 * !If the global functions are related to maps, charts, or tables. Put them somewhere else
 */

var indicatorCharts = {};
var selSettings = {
	WDPAID: null,
    iso2: null,
    regionID: null,
};

(function($){

	$(window).resize(function(){
		if($("[_echarts_instance_]").length){
			$("[_echarts_instance_]").each(function(){
				window.echarts.getInstanceById($(this).attr('_echarts_instance_')).resize();
			});
		}
	});

	$.fn.resizeMap = function(map) {
		map.resize();
	};

	$.fn.updateSectedCountry = function(property = "iso2") { 
		var countriesObj = drupalSettings.countries;
		var country = countriesObj.filter(obj => {
			return obj[property] === selSettings[property];
		});
		if (country.length){
			selSettings.bbox = country[0]["bbox"];
			selSettings.iso3 = country[0]["iso3"];
			///selSettings.iso2 = country[0]["iso2"];
			selSettings.num = country[0]["num"];
			selSettings.countryName = country[0]["name"];
			if (country[0]["region"]){
				selSettings.regionName = country[0]["region"];
				var regionName = country[0]["region"];
				selSettings.regionID = regionName.toLowerCase().replace(' ', '_');
			} else {
				selSettings.regionName = null;
				selSettings.regionID = null;
			}
		}
		return selSettings;
	}

	$.fn.sortObject = function(property, AscOrDec = 'asc') { 
		var sortOrder = 1;
		if (AscOrDec == "dec") sortOrder = -1;
		return function (a,b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}

	$.fn.addIndicatorToReport = function(indicator, type) { 
		$('div#report-link').show(200);
		numberOfIndicatorsToPrint++;
		if(type == 'Table'){
			reportIndicators[indicator+type] = activeIndicators['#data-table-'+indicator+type];
		} else {
			reportIndicators[indicator+type] = activeIndicators[indicator+type];
		}
		$('div#report-number').text(numberOfIndicatorsToPrint);
	}
	$.fn.removeIndicatorFromReport = function(indicator, type) { 
		numberOfIndicatorsToPrint--;
		delete reportIndicators[indicator+type];
		if (numberOfIndicatorsToPrint == 0){
			$('div#report-link').hide(200);
		} else {
			$('div#report-number').text(numberOfIndicatorsToPrint);
		}
	}
	$.fn.goToIndicatorReport = function() { 
		console.log(reportIndicators);
		$.jStorage.set("country-report", reportIndicators );
		var value = $.jStorage.get("country-report");
		window.location.href = "/reporting";
		
	}
	$.fn.exists = function () {
    	return this.length !== 0;
	}

	$.fn.getWindowHeight = function () {
		var height = $(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
		var adminHeight = 0;
		var menuHeight = 0;
		var gloBanHeight = 0;
		if ($('#block-mainnavigation')[0]){
			menuHeight = $("#block-mainnavigation").height();
		}
		if ($('#toolbar-administration')[0]){
			adminHeight =  79 ; //the admin toolbar is exactly 79px tall
		} 
		if ($('#toolbar-administration')[0]){
			adminHeight =  79 ; //the admin toolbar is exactly 79px tall
		} 
		height = height - (adminHeight + menuHeight);
		heightPaddingFix = adminHeight + menuHeight ; 
		$('#container-page').css('top', heightPaddingFix);
		return height; 
	}

	$.fn.createAccordion = function (id, title = '', icon = null, parent = null) {
		var parentText = ''
		var iconText = '';
		if (parent){
			parentText = " data-parent='#" + parent + "'";
		}
		if (icon){
			iconText = " <i class='" + icon + "'></i>";
		}
		var accordion = "<button class='biopama-collapse my-2 btn btn-outline-success w-100 d-flex justify-content-between' type='button' data-bs-toggle='collapse' data-bs-target='#collapse" + id + "' aria-expanded='false' aria-controls='collapse" + id + "'>" +
		"<div class='collapse-title'> " + iconText + " " + title + " </div><div class='card-collapsed'><i class='fas fa-chevron-down'></i></div><div class='card-expanded'><i class='fas fa-chevron-up'></i></div>"+
		"</button>" +
		"<div class='collapse wrapper-card text-bg-dark' id='collapse" + id + "' " + parentText + ">"+
			"<div class='mb-3 border-0'>" +
				"<div class='card-body'>" +
					"<div class='card-text p-3'>" +
					"</div>" +
				"</div>" +
			"</div>" +
		"</div>";
		return accordion; 
	}

	$.fn.biopamaReplaceTokens = function (string, token = null) {
		//if it's an object it MUST be from the CT module for tracking active variables, so we assume it's fine and these properties must be present
		//if the properties are undefined, well... you are doing something wrong.
		if (typeof token === 'object'){
			string = string.replace("[NUM]", token.num)
			.replace("[ISO2]", token.iso2)
			.replace("[ISO3]", token.iso3)
			.replace("[COUNTRYNAME]", token.countryName)
			.replace("[WDPAID]", token.WDPAID)
			.replace("[REGION]", token.regionID)
			.replace("NUM", token.num) 			//for backwards compatability (we added brackets) - to be removed at somepoint - START
			.replace("ISO2", token.iso2) 		//
			.replace("ISO3", token.iso3)		//
			.replace("WDPAID", token.WDPAID)	//
			.replace("REGION", token.regionID);	// END
		} else if (typeof token === 'string' || typeof token === 'number' ){
			var phpDateArray = ["{Y}", "{y}", "{M}", "{m}", "{D}", "{d}"];
			string = string.replace("[TOKEN]", token) //if the token is a simple string.
			///moment().format("YYYY");
			console.log(moment().format("YYYY"));
			string = string.replace("{Y}", token) //if the token is a simple string.
		} else {
			console.log("a token replacement was attempted, but something went wrong");
		}
		return string; 
	}

	$.fn.getKeys = function (obj) {
		var keys = [];
		for(var key in obj){
			keys.push(key);
		}
		return keys;
	}

	$.fn.insertBiopamaLoader = function (selector, position) {
		//$().insertBiopamaLoader(selector); 
		var loader = "<div class='mini-loader-wrapper'><div id='mini-loader'></div></div>";
		if(position == 'before'){
			$(loader).insertBefore(selector);
			return;
		} else if(position == 'after'){
			$(loader).insertAfter(selector);
			return;
		} else {
			$(selector).append(loader);
		}
	}
	$.fn.removeBiopamaLoader = function (selector) {
		//$().removeBiopamaLoader(selector); 
		//removes any and all loaders from within the target selector
		$(selector).find(".mini-loader-wrapper").remove();
	}

	$.fn.createRegionalSelect = function (nodeID) {

		var regionalSelect = "<select id='region-selector' data-placeholder='Choose a region...' class='biopama-region-selector chosen-select form-control text-bg-dark'>"+
		"<option value='' disabled selected>Select a region</option>";
		var countries = drupalSettings.countries;
		const uniqueRegions = Array.from(new Set(countries.map((item) => item.region)));

		for (var region of uniqueRegions) {
			regionalSelect += "<option value='" + region.name + "'>" + region.name + "</option>";
		}

		regionalSelect += "</select>";
		return regionalSelect; 
	}

	$.fn.createNationalSelect = function (nodeID) {
		var countries = drupalSettings.countries;
		const uniqueRegions = Array.from(new Set(countries.map((item) => item.region)));

		var nationalSelect = "<select id='country-selector' data-placeholder='Choose a country...' class='biopama-country-selector chosen-select form-control w-100 mt-2'>"+
		"<option value='' disabled selected>Select a country</option>";
		for (var region of uniqueRegions) {
			//nationalSelect += "<optgroup label='"+ region +"'></optgroup>";
			var regionalFilteredCountries = countries.filter(function (item) { return item.region == region; });
			regionalFilteredCountries.sort(function(a, b) {
				var textA = a.name.toUpperCase();
				var textB = b.name.toUpperCase();
				return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
			});

			for (var country of regionalFilteredCountries) {
				nationalSelect += "<option value='" + country.iso2 + "'>" + country.name + "</option>";
			}
		}
		nationalSelect += "</select>";
		return nationalSelect; 
	}

})(jQuery);

function resizeChart(){
	var chartWidth;
	if(jQuery(".indicator-wrapper")){
	  chartWidth = jQuery('.map-menu').outerWidth();
	  //chartWidth = document.getElementById(id).clientWidth;
	  jQuery('.indicator-chart').css('width', chartWidth);
	}
}

function getWindowHeight(){
	var height = jQuery(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
	if (jQuery('#toolbar-item-administration-tray')[0]){
		var adminHeight = jQuery('#toolbar-item-administration-tray').height() + jQuery('#toolbar-bar').height();
		jQuery('#map-container').css('top', adminHeight);
		height = height - adminHeight;
	}
	return height;
}

function buildFilter(arr, arg, filter) {
	var filter = [arg, filter];
	if (arr.length === 0) {
		return filter;
	}
	for(var i = 0; i < arr.length; i += 1) {
		filter.push(arr[i]);
	}
	return filter;
}